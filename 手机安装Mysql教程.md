# 手机安装centos9 steam&安装mysql教程
### 安装环境&准备工具
- [x] 1. 红米6Pro手机且root
- [x] 2. Linux Deploy.apk
- [x] 3. FinalShell(电脑端ssh连接工具)
- [ ] 4. Juice Ssh.apk(手机端ssh连接工具)

### mysql安装步骤
1. 安装mysql-server
```
dnf install -y mysql-server
```
2. 创建m文件存储路径，设置mysql拥有权限
```
mkdir /var/lib/mysql/data
chown -R mysql.mysql /var/lib/mysql
chmod -R 777 /var/lib/mysql
```
3. 检查数据库是否安装成功
```
mysqladmin --version
```
4. 修改mysql-server.cnf f配置文件
```
vi /etc/my.cnf.d/mysql-server.cnf
#替换文件存储路径
datadir=/var/lib/mysql/data
#嵌入式权限很讲究，设置mysql用户启动执行root用户权限
user=root
```
5. 数据库的初始化
```
mysqld --initialize-insecure --user=mysql
```
6. 启动数据库
```
 mysqld --defaults-file=/etc/my.cnf
```
7. 进入数据库
```
#查看数据库原始密码
grep 'temporary password' /var/log/mysql/mysqld.log |tail -1
#登录数据库
mysql -uroot -p
#修改密码
alter user root@localhost identified by 'root';
```
8. 授权其他机器远程连接
```
use mysql;
update user set host='%' where user='root';
Grant all privileges on root.* to 'root'@'%';
flush privileges;
select host, user from user;
```
9. navicat连接报错，修改加密规则
```
alter user 'root'@'%' IDENTIFIED WITH mysql_native_password BY 'root';
flush privileges;
```
