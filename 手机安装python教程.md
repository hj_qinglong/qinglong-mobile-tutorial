# 手机安装centos9 steam&安装python教程
### 安装环境&准备工具
- [x] 1. 红米6Pro手机且root
- [x] 2. Linux Deploy.apk
- [x] 3. FinalShell(电脑端ssh连接工具)
- [ ] 4. Juice Ssh.apk(手机端ssh连接工具)

### python安装步骤
1. 安装python所需依赖
```
yum install -y openssl-devel bzip2-devel expat-devel gdbm-devel readline-devel sqlite-devel
yum install -y zlib-devel bzip2-devel openssl-devel ncurses-devel sqlite-devel readline-devel tk-devel gcc
yum install -y libffi-devel
```
2. 下载python3.7.9安装包
```
wget https://www.python.org/ftp/python/3.7.9/Python-3.7.9.tgz -P /opt -O Python-3.7.9.tgz
tar vxf Python-3.7.9.tgz
```
3. 源码编译安装
```
cd Python-3.7.9
./configure --prefix=/usr/local/python3 --enable-optimizations
make && make install
```
4. 添加环境之软连接（(二选一)
```
ln -s /usr/local/python3/bin/python3 /usr/bin/python3
ln -s /usr/local/python3/bin/pip3 /usr/bin/pip3
```
5. 添加环境之系统变量（(二选一)
```
# vi ~/.
# 把/usr/bin/python3加到PATH那一行的后面
# PATH=$PATH:$HOME/bin:/usr/bin/python3
vi /etc/profile
# 在页末加上 export PATH=$PATH:/usr/local/python3/bin
source /etc/profile
```
6. 检测是否安装成功
```
python3 -V
pip3 -V
```
