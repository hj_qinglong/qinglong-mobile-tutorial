#手机安装centos9 steam&安装mysql教程
### 安装环境&准备工具
- [x] 1. 红米6Pro手机且root
- [x] 2. Linux Deploy.apk
- [x] 3. FinalShell(电脑端ssh连接工具)
- [ ] 4. Juice Ssh.apk(手机端ssh连接工具)

### 安装centos步骤
1. 安装Linux Deploy.apk应用
2. 左上角菜单下拉选择仓库->centos7arm
3. 右上角选择安装
4. 大约20分钟安装完成后，点击底部三角按钮启动容器
5. ssh客户端连接手机Linux系统

### 安装nodejs环境步骤
1. 安装 node.js 环境
```
#查找指定名称的软件包
dnf list nodejs
#安装nodejs
dnf -y install nodejs
#安装完成后运行 node 命令用以确定node.js是否安装完成
node -v
```
2. 安装 npm
```
#查找指定名称的软件包
dnf list npm
#安装nodejs
dnf -y install npm
#安装完成后运行 npm 命令用以确定 npm 是否安装完成
npm -v
```
3. 安装 pm2
```
# 安装 pm2
npm install pm2 -g
#安装完成后运行 pm2 命令用以确定 pm2 是否安装完成
pm2 -v
```

### 安装redis步骤
1. 安装redis
```
# 安装 redis
dnf -y install redis
#安装完成后运行 redis-server 命令启动 redis 用以确定是否安装完成
redis-server
```
2. 设置后台启动
```
vi /etc/redis/redis.conf
# daemonize no 由no改为yes
# redis-server /etc/redis/redis.conf 启动 redis
# ps -ef|grep redis 查看进程
```

### 安装nginx步骤
```
# 安装 nginx
dnf -y install nginx
# 安装完成后运行 nginx 命令启动 nginx 用以确定是否安装完成
nginx
# 查看配置文件
cat /etc/nginx/nginx.conf
```
