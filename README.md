
# 最新青龙面板拉库命令更新【2022/6/14】

## 拉库命令
> 【注意】不可同时拉多个集合库，同时拉多个集合库容易黑ip，保留一个即可。  
> 定时拉库命令可设置为 `0 */12 * * *` 每天0点和12点运行一次更新。

### 设置Proxyurl  
老版青龙,需要设置config.sh中的第18行GithubProxyUrl改为如下：
```
https://git.metauniverse-cn.com/
```

### 1. KingRan库【集合库，推荐】
```
ql repo https://github.com/KingRan/KR.git "jd_|jx_|jdCookie" "activity|backUp" "^jd[^_]|USER|utils|function|sign|sendNotify|ql|magic|JDJR"
```

### 2.Faker3【集合库】
```
ql repo https://github.com/shufflewzc/faker3.git "jd_|jx_|gua_|jddj_|jdCookie" "activity|backUp" "^jd[^_]|USER|function|utils|sendNotify|ZooFaker_Necklace.js|JDJRValidator_|sign_graphics_validate|ql|JDSignValidator" "main"
```

### 3. yyds【集合库】
**【注意】拉库前请打开青龙面板-[配置文件]第18行GithubProxyUrl="" 双引号中的内容。**
```
//YYDS
ql repo https://github.com/okyyds/yyds.git "jd_|jx_|gua_|jddj_|jdCookie" "activity|backUp" "^jd[^_]|USER|function|utils|sendNotify|ZooFaker_Necklace.js|JDJRValidator_|sign_graphics_validate|ql|JDSignValidator" "master"

//YYDS_Pure
ql repo https://github.com/okyyds/yydspure.git "jd_|jx_|gua_|jddj_|jdCookie" "activity|backUp" "^jd[^_]|USER|function|utils|sendNotify|ZooFaker_Necklace.js|JDJRValidator_|sign_graphics_validate|ql|JDSignValidator" "master"
```

### 4. smiek2121【开卡，建议】
```
ql repo https://github.com/smiek2121/scripts.git "gua_" "" "ZooFaker_Necklace.js|JDJRValidator_Pure.js|sign_graphics_validate.js|cleancart_activity.js|jdCookie.js|sendNotify.js"
```

### 5. ccwav大佬的增强版和CK检测【建议】
```
//不包含sendNotify:
ql repo https://github.com/ccwav/QLScript2.git "jd_" "sendNotify|NoUsed" "ql"

//包含sendNotify:
ql repo https://github.com/ccwav/QLScript2.git "jd_" "NoUsed" "ql|sendNotify"

```

### 6.【gys619】【集合库，可选】
```
ql repo https://github.com/gys619/jdd.git "jd_|jx_|jddj_|gua_|getJDCookie|wskey" "activity|backUp" "^jd[^_]|USER|utils|ZooFaker_Necklace|JDJRValidator_|sign_graphics_validate|jddj_cookie|function|ql|magic|JDJR|JD" "main"
```

### 7.【zero205】【集合库，拉KR即可】
```
ql repo https://github.com/zero205/JD_tencent_scf.git "jd_|jx_|jdCookie" "backUp|icon" "^jd[^_]|USER|sendNotify|sign_graphics_validate|JDJR|JDSign|ql" "main"
```

### 8.【添加订阅管理】
```
 名称：faker2
 链接：https://github.com/shufflewzc/faker2.git
 定时类型： interval
 定时规则：每1天
 白名单：jd_|jx_|gua_|jddj_|jdCookie
 黑名单：activity|backUp
 依赖文件：^jd[^_]|USER|function|utils|sendNotify|ZooFaker_Necklace.js|JDJRValidator_|sign_graphics_validate|ql|JDSignValidator
```

## 一些依赖可供参考

### 1.NodeJs下
```
crypto-js
prettytable
dotenv
jsdom
date-fns
tough-cookie
tslib
ws@7.4.3
ts-md5
jsdom
jieba
fs
form-data
json5
global-agent
png-js
@types/node
require
typescript
js-base64
axios
moment
node-fetch
```

### 2.python3下

```
requests
canvas
ping3
jieba
```

### 3.Linux下
```
bizCode
bizMsg
lxml
```

## 一些定时规则
```
*/5 * * * * ?    #每隔 5 秒执行一次
0 */1 * * * ?    #每隔 1 分钟执行一次
0 0 2 1 * ? *    #每月 1 日的凌晨 2 点执行一次
0 15 10 ? *    #MON-FRI 周一到周五每天上午 10：15 执行
0 15 10 ? 6L    #2002-2006 2002 年至 2006 年的每个月的最后一个星期五上午 10:15 执行
0 0 23 * * ?    #每天 23 点执行一次
0 0 1 * * ?    #每天凌晨 1 点执行一次
0 0 1 1 * ?     #每月 1 日凌晨 1 点执行一次
0 0 23 L * ?    #每月最后一天 23 点执行一次
0 0 1 ? * L    #每周星期天凌晨 1 点执行一次
0 26,29,33 * * * ?    #在 26 分、29 分、33 分执行一次
0 0 0,13,18,21 * * ?    #每天的 0 点、13 点、18 点、21 点都执行一次
0 0 10,14,16 * * ?    #每天上午 10 点，下午 2 点，4 点执行一次
0 0/30 9-17 * * ?    #朝九晚五工作时间内每半小时执行一次
0 0 12 ? * WED    #每个星期三中午 12 点执行一次
0 0 12 * * ?    #每天中午 12 点触发
0 15 10 ? * *    #每天上午 10:15 触发
0 15 10 * * ?    #每天上午 10:15 触发
0 15 10 * * ? *    #每天上午 10:15 触发
0 15 10 * * ?    #2005 2005 年的每天上午 10:15 触发
0 * 14 * * ?    #每天下午 2 点到 2:59 期间的每 1 分钟触发
0 0/5 14 * * ?    #每天下午 2 点到 2:55 期间的每 5 分钟触发
0 0/5 14,18 * * ?    #每天下午 2 点到 2:55 期间和下午 6 点到 6:55 期间的每 5 分钟触发
0 0-5 14 * * ?    #每天下午 2 点到 2:05 期间的每 1 分钟触发
0 10,44 14 ? 3 WED    #每年三月的星期三的下午 2:10 和 2:44 触发
0 15 10 ? * MON-FRI    #周一至周五的上午 10:15 触发
0 15 10 15 * ?    #每月 15 日上午 10:15 触发
0 15 10 L * ?    #每月最后一日的上午 10:15 触发
0 15 10 ? * 6L    #每月的最后一个星期五上午 10:15 触发
0 15 10 ? * 6L    #2002-2005 2002 年至 2005 年的每月的最后一个星期五上午 10:15 触发
0 15 10 ? * 6#3    #每月的第三个星期五上午 10:15 触发
```